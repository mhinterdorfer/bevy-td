mod plugins;

use crate::plugins::attack_plugin::AttackPlugin;
use crate::plugins::game_management_plugin::GameManagementPlugin;
use crate::plugins::tower_plugin::TowerPlugin;
use bevy::prelude::*;
use bevy::sprite::{MaterialMesh2dBundle, Mesh2dHandle};
use bevy::window::WindowTheme;
use bevy_rapier2d::prelude::*;
use rand::Rng;

const WIDTH: f32 = 720.0;
const HEIGHT: f32 = 720.0 * 16.0 / 9.0;

#[derive(Component)]
struct MainCamera;

#[derive(Component)]
struct Health {
    current_health: f32,
}

#[derive(Component)]
struct Tower;

#[derive(Component)]
struct Enemy;

#[derive(Component)]
struct RangedAttack {
    range: f32,
}

#[derive(Component)]
struct TickedTimer {
    timer: Timer,
}

#[derive(Component)]
struct Projectile {
    damage: f32,
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Bevy TD".into(),
                name: Some("bevy-td.app".into()),
                resolution: (HEIGHT, WIDTH).into(),
                prevent_default_event_handling: false,
                window_theme: Some(WindowTheme::Dark),
                resizable: false,
                enabled_buttons: bevy::window::EnabledButtons {
                    maximize: false,
                    ..Default::default()
                },
                ..default()
            }),
            ..default()
        }))
        .add_plugins(RapierPhysicsPlugin::<()>::pixels_per_meter(100.0))
        .add_plugins(RapierDebugRenderPlugin::default())
        // .add_plugins(bevy::diagnostic::FrameTimeDiagnosticsPlugin)
        // .add_plugins(bevy::diagnostic::EntityCountDiagnosticsPlugin)
        // .add_plugins(bevy::diagnostic::SystemInformationDiagnosticsPlugin)
        // .add_plugins(PerfUiPlugin)
        .insert_resource(RapierConfiguration {
            gravity: Vec2::ZERO,
            ..Default::default()
        })
        .add_systems(Startup, setup)
        .add_systems(Update, update_timers_system)
        .add_plugins(TowerPlugin)
        .add_plugins(AttackPlugin)
        .add_plugins(GameManagementPlugin)
        .run()
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    // commands.spawn(PerfUiCompleteBundle::default());

    commands.spawn((Camera2dBundle::default(), MainCamera));

    let mut rng = rand::thread_rng();

    // spawn tower for testing
    commands
        .spawn(Tower)
        .insert(MaterialMesh2dBundle {
            mesh: Mesh2dHandle(meshes.add(Capsule2d::new(25.0, 50.0))),
            material: materials.add(Color::rgb(0.0, 1.0, 0.0)),
            transform: Transform::from_xyz(0.0, 0.0, 0.0),
            ..default()
        })
        .insert(TickedTimer {
            timer: Timer::from_seconds(0.1, TimerMode::Repeating),
        })
        .insert(RangedAttack { range: 1000.0 });

    for _ in 0..10 {
        commands
            .spawn(Enemy)
            .insert(RigidBody::KinematicVelocityBased)
            .insert(Collider::capsule_y(12.5, 12.5))
            .insert(MaterialMesh2dBundle {
                mesh: Mesh2dHandle(meshes.add(Capsule2d::new(12.5, 25.0))),
                material: materials.add(Color::rgb(1.0, 0.0, 0.0)),
                transform: Transform::from_xyz(0.0, 0.0, 0.0),
                ..default()
            })
            .insert(Health {
                current_health: 100.0,
            })
            .insert(Velocity {
                linvel: Vec2 {
                    x: rng.gen_range(-1.0..1.0),
                    y: rng.gen_range(-1.0..1.0),
                } * 100.0,
                angvel: 0.0,
            });
    }
}

fn update_timers_system(time: Res<Time>, mut timer_q: Query<&mut TickedTimer>) {
    for mut timer in timer_q.iter_mut() {
        timer.timer.tick(time.delta());
    }
}
