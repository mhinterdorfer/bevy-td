use crate::{Enemy, Health, Projectile};
use bevy::prelude::*;
use bevy_rapier2d::plugin::RapierContext;

pub struct AttackPlugin;

impl Plugin for AttackPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, (projectiles_hitting_enemy_system));
    }
}

fn projectiles_hitting_enemy_system(
    rapier_context: Res<RapierContext>,
    mut enemy_q: Query<&mut Health, With<Enemy>>,
    projectile_q: Query<&Projectile>,
    mut commands: Commands,
) {
    for contact_pair in rapier_context.contact_pairs() {
        let (entity_a, entity_b) = (contact_pair.collider1(), contact_pair.collider2());
        if let (Ok(mut health), Ok(projectile)) =
            (enemy_q.get_mut(entity_a), projectile_q.get(entity_b))
        {
            health.current_health -= projectile.damage;
            commands.entity(entity_b).despawn();
        } else if let (Ok(mut health), Ok(projectile)) =
            (enemy_q.get_mut(entity_b), projectile_q.get(entity_a))
        {
            health.current_health -= projectile.damage;
            commands.entity(entity_a).despawn();
        }
    }
}
