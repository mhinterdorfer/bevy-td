use crate::Health;
use bevy::prelude::*;

pub struct GameManagementPlugin;

impl Plugin for GameManagementPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, check_killing_conditions_system);
    }
}

fn check_killing_conditions_system(
    killable_entities_q: Query<(&Health, Entity)>,
    mut commands: Commands,
) {
    for (_health, entity) in killable_entities_q
        .iter()
        .filter(|(health, _)| health.current_health <= 0.0)
    {
        commands.entity(entity).despawn()
    }
}
