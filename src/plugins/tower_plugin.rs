use crate::{Enemy, Projectile, RangedAttack, TickedTimer, Tower};
use bevy::prelude::*;
use bevy::sprite::{MaterialMesh2dBundle, Mesh2dHandle};
use bevy_rapier2d::dynamics::{RigidBody, Velocity};
use bevy_rapier2d::geometry::Collider;

pub struct TowerPlugin;

impl Plugin for TowerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, tower_shooting_system);
    }
}

fn tower_shooting_system(
    enemy_q: Query<&Transform, (With<Enemy>, Without<Tower>)>,
    tower_q: Query<(&Transform, &RangedAttack, &TickedTimer)>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let enemies: Vec<&Transform> = enemy_q.iter().collect();
    for (tower_transform, attack_properties, _) in tower_q
        .iter()
        .filter(|(_, _, timer)| timer.timer.finished())
    {
        if let Some((target, _)) = enemies
            .iter()
            .map(|enemy| {
                (
                    enemy.translation,
                    (enemy.translation - tower_transform.translation).length(),
                )
            })
            .filter(|(_enemy, length)| *length < attack_properties.range)
            .min_by(|entry_a, entry_b| entry_a.1.total_cmp(&entry_b.1))
        {
            commands
                .spawn(Projectile { damage: 10.0 })
                .insert(RigidBody::Dynamic)
                .insert(Collider::ball(10.0))
                .insert(MaterialMesh2dBundle {
                    mesh: Mesh2dHandle(meshes.add(Circle { radius: 10.0 })),
                    material: materials.add(Color::rgb(0.0, 0.0, 1.0)),
                    transform: *tower_transform,
                    ..default()
                })
                .insert(Velocity {
                    linvel: (target.xy() - tower_transform.translation.xy()).normalize() * 2000.0,
                    angvel: 0.0,
                });
        }
    }
}
